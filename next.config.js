module.exports = {
    webpack: (config, options) => {
        config.node= {
            ...config.node,
            fs: 'empty',
            child_process : 'empty',
            net : 'empty',
            tls: 'empty',
        }

        return config
    },
};
