import db from '../database/connect';
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Cookies from 'universal-cookie';

module.exports = {
    async store(username) {
        await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
        .then(async (connect) => {
            const userExists = await connect.sheetsByTitle[username];

            if (userExists) return alert('Usuário já existe.');

            const newUser = await connect.addSheet({
                headerValues: ['username', 'password', 'token', 'active', 'tag']
            });

            const newConnection = await connect
                .sheetsByTitle[newUser._rawProperties.title];
            await newConnection.updateProperties({ title: username });

            await newUser.addRow({
                username: username,
                password: '',
                token: '',
                active: false,
                tag: '',
                isCRHMember: false
            });

            return console.log('pronto');
        })
        .catch((error) => {
            return alert ('Algo deu errado:\n' + error);
        })
    },

    async auth(username, password) {
        await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
        .then(async (connect) => {
            const user = await connect.sheetsByTitle[username];
            if (!user) return alert('Usuário inexistente, inválido ou inativo.');
            
            const getRows = await user.getRows();

            const userPassword = getRows[0]._rawData[1];
            let verifyPassword = await bcryptjs.compare(password, userPassword);
            if (!verifyPassword) return alert('Usuário inexistente, inválido ou inativo.')

            const active = getRows[0]._rawData[3];
            if (active === 'FALSE') return alert('Usuário inexistente, inválido ou inativo.');
            
            const token = jwt.sign({ username },
                'NyZFCZMZYgK6H4GYeLaNUubcWComZkZnyXzxbaTvUG8EvUTuDNWO2', {
                    expiresIn: "2h" // validade de 2 horas
            });
            
            getRows[0]._rawData[2] = token;
            await getRows[0].save();
            
            const cookies = new Cookies();
            cookies.set('@pabsystem/token', token,
            { sameSite: 'strict' });

            cookies.set('@pabsystem/userid', username,
            { sameSite: 'strict' });

            console.log('pronto')

            return window.location.href = '/';
        })
        .catch((error) => {
            return alert ('Algo deu errado:\n' + error);
        })
    },
    async show(username, setUsername, setTAG) {
        await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
        .then(async (connect) => {
            const user = await connect.sheetsByTitle[username];
            if (!user) return alert('Usuário inexistente, inválido ou inativo.');
            
            const getRows = await user.getRows();
            const user_username = getRows[0]._rawData[0];
            const tag = getRows[0]._rawData[4];

            setUsername(user_username);
            setTAG(tag);
        })
        .catch((error) => {
            return console.log('Algo deu errado:\n' + error);
        })
    }
}