import axios from 'axios';
import { useState, useEffect } from 'react';
import bcryptjs from 'bcryptjs';

import Form from '../components/Form';
import Input from '../components/Input';
import Code from '../components/Code';
import registro from '../styles/Registro.module.css';

import db from '../database/connect';

const Registro = () => {
    const [code, setCode] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    useEffect(() => {
        const newCode = 'PABSystem_' + Math.random().toString(36).substr(2, 9);

        setCode(newCode);
    }, []);

    function onClickedCode() {
        navigator.clipboard.writeText(code);
        return alert('Código copiado para a área de transferência!');
    }

    async function onSubmit(e) {
        e.preventDefault();

        await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
        .then(async (connect) => {
            const user = await connect.sheetsByTitle[username];

            if (!user) return alert('Usuário inválido.');

            if (!username.trim() || !password.trim() || !confirmPassword.trim()) {
                return alert('Preencha todos os dados!');
            } 

            if (password !== confirmPassword) return alert('Senhas não coincidem!');
            
            await axios.get(`https://www.habbo.com.br/api/public/users?name=${username}`)
            .then(async (success) => {
                const motto = success.data.motto;

                if (motto !== code) {
                    return alert(`
                    Missão inválida.
                    Altere a missão do seu personagem com o
                    código fornecido abaixo.`)
                }

                const getRows = await user.getRows();

                if (getRows[0]._rawData[3] === 'TRUE') {
                    return alert('Cadastro inválido.');
                }

                bcryptjs.hash(password, 10, async (err, hash) => {
                    getRows[0]._rawData[1] = hash;
                    getRows[0]._rawData[3] = true;
                    await getRows[0].save();
                });

                return window.location.href = '/login';
            })
            .catch((error) => {
                return alert('Algo deu errado.\n' + error + '\nPor favor, tente novamente.');
            })

            return console.log('pronto');
        })
        .catch((error) => {
            return alert ('Algo deu errado:\n' + error);
        })
    }

    return (
        <>
            <div className={registro.container}>
                <Form
                    code={code}
                    type="register"
                    onSubmit={onSubmit}
                >
                    <Input
                        onChange={(e) => { setUsername(e.target.value) }}
                        type="text" 
                        placeholder="Username"
                    />
                    <Input
                        onChange={(e) => { setPassword(e.target.value) }}
                        type="password"
                        placeholder="Senha"
                    />
                    <Input
                        onChange={(e) => { setConfirmPassword(e.target.value) }}
                        type="password"
                        placeholder="Confirmar senha"
                    />
                    <Code
                        onClick={onClickedCode}
                        code={code}
                    />
                </Form>
            </div>
        </>
    )
}

export default Registro;