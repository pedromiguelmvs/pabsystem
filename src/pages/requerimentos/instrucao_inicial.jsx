import { useState, useEffect } from 'react';
import Cookies from 'universal-cookie';
import jwt from 'jsonwebtoken';
import cookie from 'cookie';
import Head from 'next/head';
import uniqid from 'uniqid';

import SideMenu from '../../components/SideMenu';
import Input from '../../components/Input';
import Button from '../../components/Button';
import LoadingButton from '../../components/LoadingButton';
import Instruction from '../../components/Instruction';
import Card from '../../components/Card';
import DataLine from '../../components/DataLine';
import Line from '../../components/Line';

import UserController from '../../controllers/UserController';
import auth from '../../middlewares/auth';
import verify_elevation_privilege from '../../middlewares/verify_elevation_privilege';

import instrucao_inicial from '../../styles/InstrucaoInicial.module.css';
import db from '../../database/connect';

import { useAuth } from '../../context/Auth';

const InstrucaoInicial = (props) => {
    const context = useAuth();
    const [approved, setApproved] = useState("");

    const [loadingBtn, setLoadingBtn] = useState(false);

    const cookies = new Cookies();

    // middleware de autenticação + verificação dos dados do user
    useEffect(async () => {
        console.log(context);
        const token = cookies.get('@pabsystem/token');
        const username = cookies.get('@pabsystem/userid');

        // try {
        //     await auth(token);
        //     await verify_elevation_privilege(
        //         username,
        //         tokens
        //     );
        // } catch (error) {
        //     return console.log(error);
        // }
    }, []);

    async function onPendent(e) {
        e.preventDefault();
        setLoadingBtn(true);

        if (!approved.trim()) {
            setLoadingBtn(false);
            return alert('Preencha, no mínimo, um (1) usuário!');
        }

        await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
        .then(async (connect) => {
            const sheet = await connect
            .sheetsByTitle['RequerimentosInstruçãoInicial'];
            
            const _id = uniqid();
            
            await sheet.addRow({
                _id,
                poster: props.username,
                posterImage: `https://www.habbo.com.br/habbo-imaging/avatarimage?user=${props.username}&action=std&direction=2&head_direction=2&gesture=std&size=l`,
                approved: approved,
                status: 'Pendente'
            });

            return window.location.href = '/requerimentos/instrucao_inicial';
        })
        .catch((error) => {
            setLoadingBtn(false);
            return console.log('Algo deu errado:\n' + error);
        })
    }

    async function approveRequirement(e) {
        await UserController.store(e.approved);

        await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
        .then(async (connect) => {
            const sheet = await connect
            .sheetsByTitle['ListagemInstruçãoInicial'];
            
            await sheet.addRow({
                poster: e.poster,
                posterImage: e.posterImage,
                approved: e.approved,
                status: 'Aprovado',
                responsible: props.username
            });
        })
        .catch((error) => {
            setLoadingBtn(false);
            return alert('Algo deu errado:\n' + error);
        });

        let post_id = e._id;
        await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
        .then(async (connect) => {
            const sheet = await connect
            .sheetsByTitle['RequerimentosInstruçãoInicial'];
            
            const rows = await sheet.getRows();

            let counter = -1;
            rows.map(async (data) => {
                counter++;
                if(data._rawData[0] === post_id) {
                    await rows[counter].delete();
                }
            })

            return window.location.href = '/requerimentos/instrucao_inicial';
        })
        .catch((error) => {
            setLoadingBtn(false);
            return alert('Algo deu errado:\n' + error);
        })
    }

    async function reproveRequirement(e) {
        await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
        .then(async (connect) => {
            const sheet = await connect
            .sheetsByTitle['ListagemInstruçãoInicial'];
            
            await sheet.addRow({
                poster: e.poster,
                posterImage: e.posterImage,
                approved: e.approved,
                status: 'Reprovado',
                responsible: props.username
            });
        })
        .catch((error) => {
            setLoadingBtn(false);
            return alert('Algo deu errado:\n' + error);
        });

        let post_id = e._id;
        await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
        .then(async (connect) => {
            const sheet = await connect
            .sheetsByTitle['RequerimentosInstruçãoInicial'];
            
            const rows = await sheet.getRows();

            let counter = -1;
            rows.map(async (data) => {
                counter++;
                if(data._rawData[0] === post_id) {
                    await rows[counter].delete();
                }
            })

            return window.location.href = '/requerimentos/instrucao_inicial';
        })
        .catch((error) => {
            setLoadingBtn(false);
            return alert('Algo deu errado:\n' + error);
        })
    }

    return (
        <>
            <Head>
                <title>PABSytem | Instrução Inicial</title>
                <meta name="description" content="Generated by create next appp" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className={instrucao_inicial.container}>
                <div className={instrucao_inicial.sideMenuContainer}>
                    <SideMenu />
                </div>
                <div className={instrucao_inicial.instructionContainer}>
                    <div>
                        <Instruction
                            number={1}
                            value="As suas informações são preenchidas automaticamente;"
                        />
                        <Instruction
                            number={2}
                            value="Você precisará de uma TAG para postar aqui;"
                        />
                        <Instruction
                            number={3}
                            value="Aproveite o formulário. 💯"
                        />
                        {(props.isCRHMember) ? (
                            <Instruction
                                number={4}
                                value="Olá, membro do CRH."
                            />
                        ) : null}
                    </div>
                </div>
                <form
                    onSubmit={onPendent}
                    className={instrucao_inicial.formContainer}
                >
                    <Input
                        type="text" 
                        value={props.username}
                        disabled={true}
                    />
                    <Input
                        type="text" 
                        value={`[${props.tag}]`}
                        disabled={true}
                    />
                    <Instruction
                        alt="User"
                        src="https://image.flaticon.com/icons/png/16/1/1176.png"
                        value={`Separe por "/". Ex.: Fulano1 / Fulano2`}
                    />
                    <Input
                        type="text" 
                        onChange={(e) => { setApproved(e.target.value) }}
                        placeholder="Nickname(s) do(s) aluno(s)"
                    />
                    {(!loadingBtn) ? (
                        <Button
                        type="submit"
                        value="Enviar"
                        />
                    ) : (
                        <LoadingButton />
                    )}
                </form>
                <div className={instrucao_inicial.cardContainer}>
                    {props.pendentList.map((todo, index) => (
                        <Card
                            key={index}
                            src={todo.posterImage}
                            username={todo.poster}
                        >
                            <DataLine
                                title="Seu nome de usuário: "
                                value={todo.poster}
                            />
                            <DataLine
                                title="Recruta(s) aprovado(s): "
                                value={todo.approved}
                            />
                            {(todo.status === 'Pendente') ? (
                                <DataLine
                                    title="Status: "
                                    dataLineType={instrucao_inicial.pendentDataline}
                                    value={todo.status}
                                />
                            ) : null}
                            {(props.isCRHMember) ? (
                                <div className={instrucao_inicial.crh_options}>
                                    <Button
                                        className={instrucao_inicial.approveBtn}
                                        onClick={() => { approveRequirement(todo) }}
                                        personalClass={true}
                                        type="button"
                                        value="Aprovar"
                                    />
                                    <Button
                                        className={instrucao_inicial.cancelBtn}
                                        onClick={() => { reproveRequirement(todo) }}
                                        personalClass={true}
                                        type="button"
                                        value="Reprovar"
                                    />
                                </div>
                            ) : (null)}
                        </Card>
                    ))}
                    {props.list.map((todo, index) => (
                        <Card
                            key={index}
                            src={todo.posterImage}
                            username={todo.poster}
                        >
                            <DataLine
                                title="Seu nome de usuário: "
                                value={todo.poster}
                            />
                            <DataLine
                                title="Recruta(s) aprovado(s): "
                                value={todo.approved}
                            />
                            <Line />
                            {(todo.status === 'Aprovado') ? (
                                <DataLine
                                    title="Status: "
                                    dataLineType={instrucao_inicial.approvedDataline}
                                    value={todo.status}
                                />
                            ) : (
                                <DataLine
                                    title="Status: "
                                    dataLineType={instrucao_inicial.reprovedDataline}
                                    value={todo.status}
                                />
                            )}
                            <DataLine
                                title="Por: "
                                value={todo.responsible}
                            />
                        </Card>
                    ))}
                </div>
            </div>
        </>
    );
}

export async function getServerSideProps(context) {
    // pegando dados do usuário
    const parsedCookies = cookie.parse(context.req.headers.cookie);

    const cookie_jar = {
        username: parsedCookies['@pabsystem/userid'],
        token: parsedCookies['@pabsystem/token']
    }

    let verifyAuth = false;
    if (!cookie_jar.token) {
        return {
            redirect: {
                permanent: false,
                destination: '/login'
            }
        }
    }

    jwt.verify(cookie_jar.token, 'NyZFCZMZYgK6H4GYeLaNUubcWComZkZnyXzxbaTvUG8EvUTuDNWO2', function(err, decoded) {
        if (err) {
            verifyAuth = false;
        } else {
            verifyAuth = true;
        }
    });
    
    const getUserDataFromDB = await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
    .then(async (connect) => {
        const user = await connect.sheetsByTitle[cookie_jar.username];
        const getRows = await user.getRows();
        
        const user_username = getRows[0]._rawData[0];
        const user_token = getRows[0]._rawData[2];
        const tag = getRows[0]._rawData[4];
        let isCRHMember = getRows[0]._rawData[5];
        let role = getRows[0]._rawData[6];
        
        if (cookie_jar.username !== user_username || cookie_jar.token !== user_token) {
            verifyAuth = false;
        }

        if (isCRHMember === 'TRUE') {
            isCRHMember = true;
        } else {
            isCRHMember = false;
        }

        return { user_username, user_token, tag, isCRHMember, role };
    })
    .catch((error) => {
        return console.log('Algo deu errado:\n' + error);
    });

    if (!verifyAuth) {
        return {
            redirect: {
                permanent: false,
                destination: '/login'
            }
        }
    }
    
    // listagem dos requerimentos
    const getRequirementsList = await db.connect('14-Ch9eDxkjOwxSxkp3F4zHIapqf4VWk6vqE9mqdQ8D8')
    .then(async (connect) => {
        const sheet = await connect
        .sheetsByTitle['ListagemInstruçãoInicial'];
        const pendentPostsSheet = await connect
        .sheetsByTitle['RequerimentosInstruçãoInicial'];
        
        const getRows = await sheet.getRows();
        const getPendentRows = await pendentPostsSheet.getRows();

        let registerList = [];
        getRows.map((register) => {
            registerList.push({
                poster: register._rawData[0],
                posterImage: register._rawData[1],
                approved: register._rawData[2],
                status: register._rawData[3],
                responsible: register._rawData[4]
            })
        })

        let registerPendentList = [];
        getPendentRows.map((register) => {
            registerPendentList.push({
                _id: register._rawData[0],
                poster: register._rawData[1],
                posterImage: register._rawData[2],
                approved: register._rawData[3],
                status: register._rawData[4]
            })
        })

        registerPendentList.reverse();
        registerList.reverse();

        return { registerPendentList, registerList }
    })
    .catch((error) => {
        return console.log('Algo deu errado:\n' + error);
    });

    return {
        props: {
            username: getUserDataFromDB.user_username,
            token: getUserDataFromDB.user_token,
            tag: getUserDataFromDB.tag,
            isCRHMember: getUserDataFromDB.isCRHMember,
            role: getUserDataFromDB.role,
            list: getRequirementsList.registerList,
            pendentList: getRequirementsList.registerPendentList,
        }
    }
}

export default InstrucaoInicial;