import React from 'react';
import Head from 'next/head';
import tags from '../../styles/Tags.module.css';

import SideMenu from '../../components/SideMenu';

const RequerimentosTags = () => {
    return(
        <>
            <Head>
                <title>TAGS | Requerimentos PABSystem</title>
                <meta name="description" content="Página destinada às TAGS." />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className={tags.container}>
                <div className={tags.sideMenuContainer}>
                    <SideMenu />
                </div>
                <div className={tags.div}>
                    <p>asdasd</p>
                </div>
            </div>
        </>
    )
}

export default RequerimentosTags;