import Head from 'next/head';
import { useState, useContext } from 'react';
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Cookies from 'universal-cookie';

import Input from '../components/Input';
import Button from '../components/Button';
import LoadingButton from '../components/LoadingButton';

import login from '../styles/Login.module.css';

import db from '../database/connect';

import { useAuth } from '../context/Auth';

const Login = () => {
    const { username, password, setUsername, setPassword } = useAuth();
    // const [username, setUsername] = useState('');
    // const [password, setPassword] = useState('');

    const [loadingBtn, setLoadingBtn] = useState(false);

    async function onSubmit(e) {
        e.preventDefault();
        setLoadingBtn(true);

        if (!username.trim() || !password.trim()) {
            setLoadingBtn(false);
            return alert('Preencha todos os campos necessários.')
        }

        await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
        .then(async (connect) => {
            const user = await connect.sheetsByTitle[username];
            if (!user) {
                setLoadingBtn(false);
                return alert('Usuário inexistente, inválido ou inativo.');
            }
            
            const getRows = await user.getRows();

            const userPassword = getRows[0]._rawData[1];
            let verifyPassword = await bcryptjs.compare(password, userPassword);
            if (!verifyPassword) {
                setLoadingBtn(false);
                return alert('Usuário inexistente, inválido ou inativo.')
            }

            const active = getRows[0]._rawData[3];
            if (active === 'FALSE') {
                setLoadingBtn(false);
                return alert('Usuário inexistente, inválido ou inativo.');
            }
            
            const token = jwt.sign({ username },
                'NyZFCZMZYgK6H4GYeLaNUubcWComZkZnyXzxbaTvUG8EvUTuDNWO2', {
                    expiresIn: "2h" // validade de 2 horas
            });
            
            getRows[0]._rawData[2] = token;
            await getRows[0].save();
            
            const cookies = new Cookies();
            cookies.set('@pabsystem/token', token,
            { sameSite: 'strict' });

            cookies.set('@pabsystem/userid', username,
            { sameSite: 'strict' });

            console.log('pronto')

            return window.location.href = '/';
        })
        .catch((error) => {
            setLoadingBtn(false);
            return alert ('Algo deu errado:\n' + error);
        })
    }

    return (
        <div className={login.container}>
            <Head>
                <title>PABSytem | Login</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <form className={login.formContainer} onSubmit={onSubmit}>
                <Input
                    onChange={(e) => { setUsername(e.target.value) }}
                    type="text" 
                    placeholder="Username"
                />
                <Input
                    onChange={(e) => { setPassword(e.target.value) }}
                    type="password"
                    placeholder="Senha"
                />
                {(!loadingBtn) ? (
                    <Button
                    type="submit"
                    value="Enviar"
                    />
                ) : (
                    <LoadingButton />
                )}
            </form>
        </div>
    );
}

export default Login;