import card from '../styles/Card.module.css';

import Shield from '../components/Shield';

const Card = (props) => {
    return (
        <div className={card.container}>
            <div className={card.profile}>
                <img
                    className={card.image}
                    src={props.src}
                    alt={props.alt}
                />
                <p>{props.username}</p>
                <Shield/>
            </div>
            <div className={card.content}>
                {props.children}
            </div>
        </div>
    )
}

export default Card;