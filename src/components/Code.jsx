import code from '../styles/Code.module.css';

const Code = (props) => {
    return (
        <div className={code.container}>
            <div className={code.info}>
                <img
                    src="https://i.imgur.com/mu7tFDA.png"
                    alt="Info"
                />
                <p>Copie o código abaixo e cole na missão do seu personagem.</p>
            </div>

            <div onClick={props.onClick} className={code.code}>
                <p>{props.code}</p>
            </div>
        </div>
    )
}

export default Code;