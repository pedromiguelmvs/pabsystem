import input from '../styles/Input.module.css';

const Input = (props) => {
    return (
        <input
            className={input.input}
            placeholder={props.placeholder}
            onChange={props.onChange}
            type={props.type}
            disabled={props.disabled}
            value={props.value}
        />
    )
}

export default Input;