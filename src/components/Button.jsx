import button from '../styles/Button.module.css';

const Button = (props) => {
    return (
        <button
            style={props.style}
            onClick={props.onClick}
            className={(props.personalClass) ? props.className : button.button}
            type={props.type}
        >{props.value}</button>
    )
}

export default Button;