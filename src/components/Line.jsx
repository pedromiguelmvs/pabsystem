import line from '../styles/Line.module.css';

const Line = () => {
    return(
        <div className={line.line}></div>
    );
}

export default Line;