import menuitem from '../styles/MenuItem.module.css';

const MenuItem = (props) => {

    const onClick = () => {
        return window.location.href = props.href;
    }

    return (
        <div onClick={onClick} className={menuitem.container}>
            <img
                src={props.src}
                alt={props.alt}
            />
            <p>{props.paragraph}</p>
        </div>
    )
}

export default MenuItem;