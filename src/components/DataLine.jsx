import dataline from '../styles/DataLine.module.css';

const DataLine = (props) => {
    return (
        <div className={dataline.p}>
            <span>{props.title}</span>
            <p className={props.dataLineType}>{props.value}</p>
        </div>
    )
}

export default DataLine;