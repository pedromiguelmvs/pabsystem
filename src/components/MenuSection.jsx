import { useState } from 'react';

import menusection from '../styles/MenuSection.module.css';

const MenuSection = (props) => {
    const [showElements, setShowElements] = useState(false);
    const [arrow, setArrow] = useState("https://image.flaticon.com/icons/png/16/32/32195.png");
    const [altArrow, setAltArrow] = useState("ArrowDown");

    function onClick() {
        if (showElements) {
            setShowElements(false);
            // set down
            setArrow("https://image.flaticon.com/icons/png/16/32/32195.png");
            setAltArrow("ArrowDown");
        } else {
            setShowElements(true);
            // set right
            setArrow("https://image.flaticon.com/icons/png/512/271/271228.png");
            setAltArrow("ArrowRight");
        }
    }
    
    return (
        <>
            <div
                className={menusection.container}
                onClick={onClick}
            >
                <div className={menusection.align} >
                    <p>{props.title}</p>
                    <img
                        src={arrow}
                        alt={altArrow}
                    />
                </div>
            </div>
            <div className={menusection.elements}>
                {(showElements) ? (
                    props.children
                ) : null}
            </div>
        </>
    )
}

export default MenuSection;