import logo from '../styles/Logo.module.css';

const Logo = () => {
    return (
        <div className={logo.container}>
            <p className={logo.paragraph}>
                <span className={logo.span}>PAB</span>
                System
            </p>
        </div>
    )
}

export default Logo;