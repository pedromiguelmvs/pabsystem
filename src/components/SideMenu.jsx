import sidemenu from '../styles/SideMenu.module.css';

import Logo from '../components/Logo';
import MenuItem from '../components/MenuItem';
import MenuSection from '../components/MenuSection';

const SideMenu = (props) => {
    return (
        <div className={sidemenu.container}>
            <Logo />
            <div>
                <MenuItem
                    alt="User"
                    src="https://image.flaticon.com/icons/png/16/456/456212.png"
                    paragraph="Início"
                    href="/"
                />
                <MenuSection title="Requerimentos">
                    <MenuItem
                        alt="InitialInstructionPage"
                        src="https://image.flaticon.com/icons/png/16/565/565814.png"
                        paragraph="Instrução Inicial"
                        href="/requerimentos/instrucao_inicial"
                    />
                    <MenuItem
                        alt="OficialRequirementsPage"
                        src="https://image.flaticon.com/icons/png/16/565/565814.png"
                        paragraph="Instrução Inicial"
                        href="/requerimentos/oficiais"
                    />
                    <MenuItem
                        alt="TagsPage"
                        src="https://image.flaticon.com/icons/png/16/633/633988.png"
                        paragraph="TAGs"
                        href="/requerimentos/tags"
                    />
                </MenuSection>
                <MenuSection title="Listagens"></MenuSection>
            </div>
            <div></div>
        </div>
    )
}

export default SideMenu;