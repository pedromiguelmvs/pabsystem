import loading_button from '../styles/LoadingButton.module.css';

const LoadingButton = () => {
    return (
        <button
            type="button"
            disabled={true}
            className={loading_button.container}
        >
            <div className={loading_button['lds-dual-ring']}></div>
        </button>
    )
}

export default LoadingButton;