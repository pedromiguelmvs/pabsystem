import shield from '../styles/Shield.module.css';

const Shield = (props) => {
    return(
        <div className={shield.container}>
            <img
                src="https://i.imgur.com/3KyqoOr.png"
                alt="developer"
            />
            <p>Desenvolvedor</p>
        </div>
    );
}

export default Shield;