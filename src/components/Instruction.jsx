import instruction from '../styles/Instruction.module.css';

const Input = (props) => {
    return (
        <div className={instruction.container}>
            <div className={instruction.numberContainer}>
                {props.number || <img src={props.src} alt={props.alt} />}
            </div>
            <div className={instruction.text}>
                {props.value}
            </div>
        </div>
    )
}

export default Input;