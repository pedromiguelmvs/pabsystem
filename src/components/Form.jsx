import form from '../styles/Form.module.css';
import Button from '../components/Button';

const Form = (props) => {
    return (
        <>
            <form onSubmit={props.onSubmit} className={form.form}>
                <img 
                    className={form.logo}
                    src="https://i.imgur.com/fVbeoYy.png"
                    alt="pab-logo"
                />
                {props.children}
                <Button type="submit" value="Enviar" />
            </form>
        </>
        
    )
}

export default Form;