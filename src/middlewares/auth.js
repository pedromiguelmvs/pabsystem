import jwt from 'jsonwebtoken';

async function auth(token) {
    let verifyAuth = false;
    
    if (!token) {
        return {
            redirect: {
                permanent: false,
                destination: '/login'
            }
        }
    }

    jwt.verify(token, 'NyZFCZMZYgK6H4GYeLaNUubcWComZkZnyXzxbaTvUG8EvUTuDNWO2', function(err, decoded) {
        if (err) {
            verifyAuth = false;
        } else {
            verifyAuth = true;
        }
    });

    if (!verifyAuth) {
        return {
            redirect: {
                permanent: false,
                destination: '/login'
            }
        }
    }
};

export default auth;
