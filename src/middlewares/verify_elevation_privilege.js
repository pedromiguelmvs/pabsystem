import db from '../database/connect';

async function verify_elevation_privilege(
    username, 
    token) {
    await db.connect('1SfB5I8IvMSR6bnq7BAANQtnbMsm5cBqhGNm-miJVGxk')
    .then(async (connect) => {
        const user = await connect.sheetsByTitle[username];
        if (!user) return window.location.href = '/login';
        
        const getRows = await user.getRows();

        const user_username = getRows[0]._rawData[0];
        const user_token = getRows[0]._rawData[2];

        // verificando elevação de privilégio
        if (user_username !== username || user_token !== token) {
            return window.location.href = "/login";
        }

        return { user_username, user_token };
    })
    .catch((error) => {
        return console.log('Algo deu errado:\n' + error);
    })
}

export default verify_elevation_privilege;