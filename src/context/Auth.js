import React, { createContext, useState, useContext } from "react";

const AuthContext = createContext({});

export default function AuthProvider({ children }) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    return (
        <AuthContext.Provider
            value={{
                signed: true,
                username,
                setUsername,
                password,
                setPassword
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

export const useAuth = () => useContext(AuthContext);