import { useContext, useState, createContext } from 'react';

export const UsernameContext = createContext({});

export const UsernameProvider = (props) => {
    const [globalUsername, setGlobalUsername] = useState("");

    return (
        <UsernameContext.Provider value={{ globalUsername, setGlobalUsername }}>
            {props.children}
        </UsernameContext.Provider>
    );
}

export const useGlobalUsername = () => useContext(UsernameContext);