import { GoogleSpreadsheet } from 'google-spreadsheet';

module.exports = {
    async connect(url) {
        const connect = new GoogleSpreadsheet(url);

        await connect.useServiceAccountAuth({
            client_email: process.env.NEXT_PUBLIC_GOOGLE_SERVICE_ACCOUNT_EMAIL,
            private_key: process.env.NEXT_PUBLIC_GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n'),
        });

        await connect.loadInfo();
        
        return connect;
    }
}